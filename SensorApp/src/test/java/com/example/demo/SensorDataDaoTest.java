package com.example.demo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.login.domain.repository.SensorDataDao;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SensorDataDaoTest {
	@Autowired
	SensorDataDao sdao;


    // センサデータ検索テスト
	@Test
    public void sensorDataFindTest() {
		//簡単のためテストデータは用意済みとする
		assertEquals(sdao.sensorDataFindOne("610c23987337ef0e8f106373").getUserName(), "testuser2");
        assertEquals(sdao.sensorDataFindOne("610c23987337ef0e8f106373").getSensorType(), "test");
        assertEquals(sdao.sensorDataFindOne("610c23987337ef0e8f106373").getSensorValue(), "200");
        assertEquals(sdao.sensorDataFindOne("610c23987337ef0e8f106373").getSensorPos(), "(1,1)");
        assertEquals(sdao.sensorDataFindOne("610c23987337ef0e8f106373").getTimestamp().toString(), "Tue Dec 29 07:27:00 JST 2020");
    }

	//他テストはTBC
}


