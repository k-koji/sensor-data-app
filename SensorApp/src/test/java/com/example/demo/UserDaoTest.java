package com.example.demo;

import static org.junit.Assert.*;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.repository.UserDao;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder (MethodSorters.NAME_ASCENDING)
public class UserDaoTest {


	@Autowired
    UserDao dao;

    // ユーザ追加テスト
	@Test
    public void userInsetTest() {
    	User user = new User();
    	user.setUserName("testuser");
    	user.setPassword("testuser");
    	user.setMailAddress("testuser@testuser");

    	dao.userInsertOne(user);

        assertEquals(dao.userFindOne("testuser"), user);
    }

    // ユーザ更新テスト
	@Test
    public void userUpdateTest() {
		User user = new User();
		user.setUserName("testuser");
    	user.setPassword("testuser");
    	user.setMailAddress("testuser@");

    	dao.userUpdateOne(user);

        assertEquals(dao.userFindOne("testuser"), user);
    }

	// ユーザ削除テスト
	@Test
    public void userDeleteTest() {
		User user = new User();
		user.setUserName("testuser");
    	user.setPassword("testuser");
    	user.setMailAddress("testuser@");

    	assertEquals(dao.userDeleteOne(user), 1);
	}
}


