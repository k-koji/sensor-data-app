package com.example.demo.login.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;
import com.example.demo.login.domain.model.SensorDataUpdateForm;
import com.example.demo.login.domain.model.SignupForm;
import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.service.SensorService;
import com.example.demo.login.domain.service.UserService;


@Controller
public class HomeController {
	@Lazy
    @Autowired
    UserService userService;

	@Lazy
    @Autowired
    SensorService sensorService;

    /**
     * ホーム画面のGET用メソッド.
     */
    @GetMapping("/home")
    public String getHome(Model model) {
    	model.addAttribute("contents", "login/home::home_contents");

        return "login/homeLayout";
    }

    /**
     * ユーザアカウントのGET用メソッド.
     */
    @GetMapping("/user")
    public String getUser(Model model) {
    	model.addAttribute("contents", "login/user::user_contents");

    	//自身のユーザ情報を取得する
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        User user = userService.userFindOne(userName);
        model.addAttribute("user", user);

    	return "login/homeLayout";
    }

    /**
     * ユーザ詳細画面のGET用メソッド
     */
    @GetMapping("/userDetail")
    public String getUserDetail(@ModelAttribute SignupForm form, Model model) {
		model.addAttribute("contents", "login/userDetail::userDetail_contents");

    	//自身のユーザ情報を取得する
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

		if (userName != null && userName.length() > 0) {
			//ユーザ検索処理
			User user = userService.userFindOne(userName);

			form.setUserName(user.getUserName());
			form.setPassword(""); //パスワードは非表示
			form.setMailAddress(user.getMailAddress());

			model.addAttribute("signupForm", form);
		}

    	return "login/homeLayout";
    }

    /**
     * ユーザ更新用処理
     */
    @PostMapping(value = "/userDetail", params = "update")
    public String postUserDetailUpdate(@ModelAttribute @Validated SignupForm form, BindingResult bindingResult, Model model) {

    	//入力でエラーがあればユーザ詳細画面に戻る
    	if(bindingResult.hasErrors()) {
    		return getUserDetail(form, model);
    	}

    	User user = new User();
    	user.setUserName(form.getUserName());
    	user.setPassword(form.getPassword());
    	user.setMailAddress(form.getMailAddress());

    	//更新実行
    	boolean result = userService.userUpdateOne(user);

    	if (result == true) {
    		model.addAttribute("result", "更新成功");
    	} else {
    		model.addAttribute("result", "更新失敗");
    	}

    	return getUser(model);
    }

    /**
     * ユーザ削除用処理
     */
    @PostMapping(value = "/userDetail", params = "delete")
    public String postUserDetailDelete(@ModelAttribute @Validated SignupForm form, BindingResult bindingResult, Model model) {

    	//入力でエラーがあればユーザ詳細画面に戻る
    	if(bindingResult.hasErrors()) {
    		return getUserDetail(form, model);
    	}

    	model.addAttribute("contents", "login/result::result_contents");

    	User user = new User();
    	user.setUserName(form.getUserName());
    	user.setPassword(form.getPassword());

    	//削除実行
    	boolean result = userService.userDeleteOne(user);

      	if (result == false) {
       		model.addAttribute("result", "ユーザ削除失敗");
      		return getUser(model);
    	}

    	return "redirect:/login";
    }

    /**
     * センサデータ画面のGET用メソッド.
     */
    @GetMapping("/sensorDataList")
    public String getSensorDataList(@ModelAttribute SearchSensorDataForm form, Model model) {
    	model.addAttribute("contents", "sensor/sensorDataList::sensorDataList_contents");
    	model.addAttribute("sensorDataCount", 0); //センサデータ件数初期化

    	//ログインユーザの取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

		if (userName != null && userName.length() > 0 && form.getStartTimestamp() != null && form.getEndTimestamp() != null) {
        //if (userName != null && userName.length() > 0) {
        	SearchSensorDataForm searchSensorData = new SearchSensorDataForm();
			searchSensorData.setUserName(userName);
			searchSensorData.setSensorType(form.getSensorType());
			searchSensorData.setSensorValue(form.getSensorValue());
			searchSensorData.setSensorPos(form.getSensorPos());
			searchSensorData.setStartTimestamp(form.getStartTimestamp());
			searchSensorData.setEndTimestamp(form.getEndTimestamp());

			//センサデータ一覧の生成
			List<SensorData> sensorDataList = sensorService.sensorDataList(searchSensorData);
			model.addAttribute("sensorDataList", sensorDataList);

			//センサデータ件数
			model.addAttribute("sensorDataCount", sensorDataList.size());
		}

    	return "login/homeLayout";
    }

    /**
     * センサデータ詳細画面のGET用メソッド
     */
    @GetMapping(value = "/sensorDataDetail/{id:.+}")
    public String getSensorDataDetail(@ModelAttribute @PathVariable("id") String id, Model model) {
		model.addAttribute("contents", "sensor/sensorDataDetail::sensorDataDetail_contents");

		//ユーザ検索処理
		SensorData sensordata = sensorService.sensorDataFindOne(id);

		model.addAttribute("sensorDataUpdateForm", sensordata);

    	return "login/homeLayout";
    }

    /**
	 * センサデータ検索用メソッド
	 */
	@PostMapping(value = "/sensorDataList", params = "search")
	public String postSensorDataSearch(@ModelAttribute @Validated SearchSensorDataForm form, BindingResult bindingResult, Model model) {
		return getSensorDataList(form, model);
	}

	/**
	 * センサデータ更新用メソッド
	 */
	 @PostMapping(value = "/sensorDataDetail", params = "update")
	 public String postSensorDataUpdate(@ModelAttribute @Validated SensorDataUpdateForm form, BindingResult bindingResult, Model model) {
			model.addAttribute("contents", "sensor/sensorDataDetail::sensorDataDetail_contents");
			boolean result = false;

			//入力でエラーがあればユーザ詳細画面に戻る
	    	if(bindingResult.hasErrors()) {
	    		model.addAttribute("sensorDataUpdateForm", form);

	    		return "login/homeLayout";
	    	}

			SensorData sensordata = sensorService.sensorDataFindOne(form.getId());
			sensordata.setSensorType(form.getSensorType());
			sensordata.setSensorValue(form.getSensorValue());
			sensordata.setSensorPos(form.getSensorPos());

			result = sensorService.sensorDataUpdateOne(sensordata);
			if (result) {
	    		model.addAttribute("result", "センサデータ更新成功");
			} else {
				model.addAttribute("result", "センサデータ更新失敗");

				return getSensorDataDetail(sensordata.getId(), model);
			}

			return getSensorDataDetail(sensordata.getId(), model);
	 }

    /**
     * センサデータ削除用メソッド
     */
	 @PostMapping(value = "/sensorDataDetail", params = "delete")
	 public String postSensorDataDelete(@ModelAttribute SensorDataUpdateForm form, BindingResult bindingResult, Model model) {
		 	model.addAttribute("contents", "sensor/sensorDataDetail::sensorDataDetail_contents");
			boolean result = false;

			//ログインユーザの取得
	    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
	        String userName = auth.getName();

			//削除実行
	    	result = sensorService.sensorDataDeleteOne(form.getId(), userName);
	    	if (result == false) {
				model.addAttribute("result", "センサデータ削除失敗");

				return getSensorDataDetail(form.getId(), model);
			}

		 return "redirect:/sensorDataList";
	 }

    /**
     * ログアウト画面用のPOST用メソッド.
     */
    @PostMapping("/logout")
    public String postLogout() {

        //ログイン画面にリダイレクト
        return "redirect:/login";
    }

}
