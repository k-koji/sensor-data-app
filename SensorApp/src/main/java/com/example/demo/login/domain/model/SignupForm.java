package com.example.demo.login.domain.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import lombok.Data;

@Data
public class SignupForm {
	@NotBlank
	private String userName;

	@NotBlank
	@Pattern(regexp = "^[a-zA-A0-9]+$")
	private String password;

	@NotBlank
	@Email
	private String mailAddress;
}