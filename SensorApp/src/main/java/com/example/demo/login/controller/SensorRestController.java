package com.example.demo.login.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;
import com.example.demo.login.domain.model.SensorDataInsertForm;
import com.example.demo.login.domain.model.SensorDataUpdateForm;
import com.example.demo.login.domain.service.SensorRestService;

@RestController
public class SensorRestController {
	@Lazy
	@Autowired
	SensorRestService service;

	/**
	 * センサデータ取得
	 */
	@GetMapping("/rest/sensordata/get")
	public List<SensorData> sensorDataFind(@RequestBody @Validated SearchSensorDataForm searchSensorData, BindingResult bindingResult) {
		//入力でエラーがあればエラーメッセージを返す
		if(bindingResult.hasErrors()) {
			//戻り値がList型なので、単純にエラーメッセージを返せない。TBC
		}

		//ログインユーザの取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        searchSensorData.setUserName(userName);

		return service.sensorDataList(searchSensorData);
	}

	/**
	 * センサデータ登録
	 */
	@PostMapping("/rest/sensordata/insert")
	public String postSensorDataOne(@RequestBody @Validated SensorDataInsertForm form, BindingResult bindingResult) {
		//入力でエラーがあればエラーメッセージを返す
		if(bindingResult.hasErrors()) {
			return bindingResult.toString();
		}

		//ログインユーザの取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

    	SensorData sensordata = new SensorData();
		sensordata.setUserName(userName);

		sensordata.setSensorType(form.getSensorType());
		sensordata.setSensorValue(form.getSensorValue());
		sensordata.setSensorPos(form.getSensorPos());
		sensordata.setTimestamp(form.getTimestamp());

		if(service.sensorDataInsertOne(sensordata)) {  //登録成功
			return "{\"result\":\"OK\"}";
		}

		return "{\"result\":\"NG\"}";
	}

	/**
	 * センサデータ更新
	 */
	@PostMapping("/rest/sensordata/update")
	public String sensorDataUpdateOne(@RequestBody @Validated SensorDataUpdateForm form, BindingResult bindingResult) {
		//入力でエラーがあればエラーメッセージを返す
		if(bindingResult.hasErrors()) {
			return bindingResult.toString();
		}

		SensorData sensordata = service.sensorDataFindOne(form.getId());
		sensordata.setSensorType(form.getSensorType());
		sensordata.setSensorValue(form.getSensorValue());
		sensordata.setSensorPos(form.getSensorPos());

		System.out.println("aaaaaa "+ sensordata);

    	if(service.sensorDataUpdateOne(sensordata)) {
			return "{\"result\":\"OK\"}";
		}

		return "{\"result\":\"NG\"}";
	}

	/**
	 * センサデータ削除
	 */
	@DeleteMapping("/rest/sensordata/delete/{id:.+}")
	public String sensorDataDeleteOne(@PathVariable("id") String id) {
		//ログインユーザの取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

    	if(service.sensorDataDeleteOne(id, userName)) {
    		return "{\"result\":\"OK\"}";
    	}

		return "{\"result\":\"NG\"}";
	}
}