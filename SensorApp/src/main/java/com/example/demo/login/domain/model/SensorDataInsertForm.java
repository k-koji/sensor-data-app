package com.example.demo.login.domain.model;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Document(collection = "sensor_data")
public class SensorDataInsertForm implements Serializable{
	@NotBlank
	private String sensorType;
	@NotBlank
    private String sensorValue;
	@NotBlank
    private String sensorPos;
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
	private Date timestamp;
}
