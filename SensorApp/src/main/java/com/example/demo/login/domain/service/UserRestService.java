package com.example.demo.login.domain.service;

import com.example.demo.login.domain.model.User;

public interface UserRestService {

	/**
	 * ユーザ情報検索メソッド
	 */
	public User userFindOne(String userName);

	/**
	 * ユーザ追加メソッド
	 */
	public boolean userInsertOne(User user);

	/**
	 * ユーザ更新メソッド
	 */
	public boolean userUpdateOne(User user);

	/**
	 * ユーザ削除メソッド
	 */
	public boolean userDeleteOne(User user);


}