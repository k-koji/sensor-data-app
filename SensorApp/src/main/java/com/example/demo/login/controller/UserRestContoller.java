package com.example.demo.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.login.domain.model.SignupForm;
import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.service.UserRestService;

@RestController
public class UserRestContoller {
	@Lazy
	@Autowired
	UserRestService service;

	/**
	 * ユーザ情報取得
	 */
	@GetMapping("/rest/user/get")
	public String getUser() {
		//ログインユーザを取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        User user = service.userFindOne(userName);

        // passwordは結果に含めない仕様とする
        return "{\"userName\":\"" + user.getUserName() + "\",\"mailAddress\":\"" + user.getMailAddress() + "\"}";
	}

	/**
	 * ユーザ登録
	 */
	@PostMapping("/rest/user/insert")
	public String postUserOne(@RequestBody @Validated SignupForm form, BindingResult bindingResult) {
		User user = new User();
		user.setUserName(form.getUserName());
		user.setPassword(form.getPassword());
		user.setMailAddress(form.getMailAddress());

		//入力でエラーがあればエラーメッセージを返す
		if(bindingResult.hasErrors()) {
			return bindingResult.toString();
		}

		if (service.userFindOne(user.getUserName()) == null) { //ユーザ名が存在しない場合登録処理
			service.userInsertOne(user);
			if (service.userFindOne(user.getUserName()) != null) { //登録成功
				return "{\"result\":\"OK\"}";
			}
		}

		return "{\"result\":\"NG\"}";
	}

	/**
	 * ユーザ更新
	 */
	@PutMapping("/rest/user/update")
	public String userUpdateOne(@RequestBody @Validated SignupForm form, BindingResult bindingResult) {
		//ログインユーザを取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();

		User user = new User();
		user.setUserName(userName);
		user.setPassword(form.getPassword());
		user.setMailAddress(form.getMailAddress());

		//入力でエラーがあればエラーメッセージを返す
		if(bindingResult.hasErrors()) {
			return bindingResult.toString();
		}

		if (service.userUpdateOne(user)) {
				return "{\"result\":\"OK\"}";
		}

		return "{\"result\":\"NG\"}";
	}

	/**
	 * ユーザ削除
	 */
	@DeleteMapping("/rest/user/delete")
	public String userDeleteOne(User user) {
		//ログインユーザ名とパスワードを取得
    	Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String userName = auth.getName();
        UserDetails principal = (UserDetails) auth.getPrincipal();
        String password = principal.getPassword();

		user.setUserName(userName);
		user.setPassword(password);

		if (service.userDeleteOne(user)) { //削除成功
			return "{\"result\":\"OK\"}";
		}

		return "{\"result\":\"NG\"}";
	}
}