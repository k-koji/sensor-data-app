package com.example.demo.login.domain.service.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;
import com.example.demo.login.domain.repository.SensorDataDao;
import com.example.demo.login.domain.service.SensorRestService;


@Transactional
@Service
public class SensorRestServiceImpl implements SensorRestService {

    @Autowired
	SensorDataDao sdao;

	@Override
	public SensorData sensorDataFindOne(String id) {
		return sdao.sensorDataFindOne(id);
	}

	@Override
	public List<SensorData> sensorDataList(SearchSensorDataForm searchSensorData) {
		return sdao.sensorDataList(searchSensorData);
	}

	@Override
	public boolean sensorDataInsertOne(SensorData sensorData) {
		//TBC データが正常にinsertできたか、findできない。センサデータだと、id列以外は全て同じデータが入る可能性もある。
		sdao.sensorDataInsertOne(sensorData);

		return true;
	}

	@Override
	public boolean sensorDataUpdateOne(SensorData sensorData) {
		boolean result = false;

		if (sdao.sensorDataUpdateOne(sensorData) != null) { //更新成功
			result = true;
		}

		return result;
	}

	@Override
	public boolean sensorDataDeleteOne(String id, String userName) {
		boolean result = false;

		if(sdao.sensorDataDeleteOne(id, userName) == 1) {  //削除成功
			result = true;
		}

		return result;
	}
}
