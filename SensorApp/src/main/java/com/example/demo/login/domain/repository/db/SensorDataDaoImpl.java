package com.example.demo.login.domain.repository.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;
import com.example.demo.login.domain.repository.SensorDataDao;
import com.mongodb.client.result.DeleteResult;

@Repository
public class SensorDataDaoImpl implements SensorDataDao {
	@Autowired
	private MongoTemplate template;


	@Override
	@Transactional(readOnly = false)
	public void sensorDataInsertOne(SensorData sensordata) throws DataAccessException {
		System.out.println(sensordata);
		template.save(sensordata);
	}

	@Override
	public SensorData sensorDataFindOne(String id) throws DataAccessException {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		SensorData sensordata = template.findOne(query, SensorData.class);

		return sensordata;
	}

	@Override
	public List<SensorData> sensorDataList(SearchSensorDataForm searchSensorData) throws DataAccessException {
		Query query = new Query();

		// 最新のタイムスタンプでソート
		query.with(new Sort(Sort.Direction.DESC, "timestamp"));

		// ログインユーザのデータを取得
		query.addCriteria(Criteria.where("userName").is(searchSensorData.getUserName()));

		// 検索条件にセンサタイプの指定があれば、結果を絞り込む
		if (searchSensorData.getSensorType() != null && searchSensorData.getSensorType().length() > 0) {
			query.addCriteria(Criteria.where("sensorType").is(searchSensorData.getSensorType()));
		}

		// 検索条件にセンサデータの範囲指定があれば、結果を絞り込む TBC

		// 検索条件に位置情報の範囲指定があれば、結果を絞り込む TBC

		// 検索条件にタイムスタンプの範囲指定があれば、結果を絞り込む
		if (searchSensorData.getStartTimestamp() != null &&  searchSensorData.getEndTimestamp() != null) {
			query.addCriteria(new Criteria().andOperator(Criteria.where("timestamp").gte(searchSensorData.getStartTimestamp()), Criteria.where("timestamp").lte(searchSensorData.getEndTimestamp())));
		}

		List<SensorData> sensorDataAll = template.find(query, SensorData.class);

		return sensorDataAll;
	}


	@Override
	@Transactional(readOnly = false)
	public SensorData sensorDataUpdateOne(SensorData sensordata) throws DataAccessException {
		//更新前のセンサデータ情報の取得
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(sensordata.getId()));

		Update updateSensorData = new Update();
		updateSensorData.set("sensorType", sensordata.getSensorType());
		updateSensorData.set("sensorValue", sensordata.getSensorValue());
		updateSensorData.set("sensorPos", sensordata.getSensorPos());

		sensordata = template.findAndModify(query, updateSensorData, SensorData.class);

		return sensordata;
	}


	@Override
	public int sensorDataDeleteOne(String id, String userName) throws DataAccessException {
		Query query = new Query();

		// ログインユーザのデータを取得
		query.addCriteria(Criteria.where("userName").is(userName));

		// idの取得
		query.addCriteria(Criteria.where("_id").is(id));
		DeleteResult result = template.remove(query, SensorData.class);

		return (int)result.getDeletedCount();
	}
}