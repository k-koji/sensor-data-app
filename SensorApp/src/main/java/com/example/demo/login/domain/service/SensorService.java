package com.example.demo.login.domain.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;
import com.example.demo.login.domain.repository.SensorDataDao;

@Service
public class SensorService {

	@Autowired
	SensorDataDao sdao;

	/**
	 * センサデータ検索メソッド
	 */
	public SensorData sensorDataFindOne(String id) {
		return sdao.sensorDataFindOne(id);
	}

	/**
	 * センサデータリスト取得メソッド
	 */
	public List<SensorData> sensorDataList(SearchSensorDataForm searchSensorData) {
		return sdao.sensorDataList(searchSensorData);
	}

	/**
	 * センサデータ更新メソッド
	 */
	public boolean sensorDataUpdateOne(SensorData sensorData) {
		boolean result = false;

		if (sdao.sensorDataUpdateOne(sensorData) != null) { //更新成功
			result = true;
		}

		return result;
	}

	/**
	 * センサデータ削除メソッド
	 */
	public boolean sensorDataDeleteOne(String id, String userName) {
		boolean result = false;

		if (sdao.sensorDataDeleteOne(id, userName) == 1) {
			result = true;
		}

		return result;
	}

}