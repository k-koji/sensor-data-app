package com.example.demo.login.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.login.domain.model.SignupForm;
import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.service.UserService;


@Controller
public class SignupController {
	@Lazy
	@Autowired
	private UserService userService;

    /**
     * ユーザ登録画面のGET用メソッド.
     */
	@GetMapping("/signup")
	public String getSignUp(@ModelAttribute SignupForm form, Model model) {

		return "login/signup";
	}

    /**
     * ユーザ登録画面のPOST用メソッド.
     */
	@PostMapping(value = "/signup", params = "resister")
	public String postSignUp(@ModelAttribute @Validated SignupForm form, BindingResult bindingResult, Model model) {

		//入力でエラーがあればユーザ登録画面に戻る
		if(bindingResult.hasErrors()) {
			return getSignUp(form, model);
		}

		//ユーザ登録処理
		User user = new User();
		user.setUserName(form.getUserName());
		user.setPassword(form.getPassword());
		user.setMailAddress(form.getMailAddress());

		boolean result = userService.userInsertOne(user);
		if (result) {
			model.addAttribute("result", "ユーザ登録成功。キャンセルボタンからログイン画面へお戻りください。");
		} else {
			model.addAttribute("result", "ユーザ登録失敗");
			return getSignUp(form, model);
		}

		return getSignUp(form, model);
	}

	/**
	 * ユーザ登録キャンセル時のPOSTメソッド
	 */
	@PostMapping(value = "/signup", params = "cansel")
	public String postCncel() {

		//ログイン画面にリダイレクト
        return "redirect:/login";
	}

}