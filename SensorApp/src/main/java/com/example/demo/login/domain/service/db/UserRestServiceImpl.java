package com.example.demo.login.domain.service.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.repository.UserDao;
import com.example.demo.login.domain.service.UserRestService;

@Transactional
@Service
public class UserRestServiceImpl implements UserRestService {

	@Autowired
	UserDao dao;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public User userFindOne(String userName) {
		return dao.userFindOne(userName);
	}

	@Override
	public boolean userInsertOne(User user) {
		boolean result = false;

		if (dao.userFindOne(user.getUserName()) == null) { //ユーザ名が存在しない場合登録処理
			dao.userInsertOne(user);
			if (dao.userFindOne(user.getUserName()) != null) { //登録成功
				result = true;
			}
		}

		return result;
	}

	@Override
	public boolean userUpdateOne(User user) {
		boolean result = false;

		if (dao.userUpdateOne(user) != null) { //更新成功
			result = true;
		}

		return result;
	}

	@Override
	public boolean userDeleteOne(User user) {
		boolean result = false;

		//パスワードが正しい場合ユーザを削除する
		String encodedPassword = userFindOne(user.getUserName()).getPassword();

		if(user.getPassword().equals(encodedPassword)) {
			if (dao.userDeleteOne(user) == 1) { //削除成功
				result = true;
			}
		}

		return result;
	}
}
