package com.example.demo.login.domain.model;

import java.io.Serializable;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "sensor_data")
public class SensorData implements Serializable{
	private String id;
    private String userName;
	private String sensorType;
    private String sensorValue;
    private String sensorPos;
    private Date timestamp;
}
