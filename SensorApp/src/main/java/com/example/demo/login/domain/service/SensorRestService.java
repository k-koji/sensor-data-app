package com.example.demo.login.domain.service;

import java.util.List;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;

public interface SensorRestService {

	/**
	 * センサデータ検索メソッド
	 */
	public SensorData sensorDataFindOne(String id);

	/**
	 * センサデータリスト取得メソッド
	 */
	public List<SensorData> sensorDataList(SearchSensorDataForm searchSensorData);

	/**
	 * センサデータ追加メソッド
	 */
	public boolean sensorDataInsertOne(SensorData sensordata);

	/*
	 * センサデータ更新メソッド
	 */
	public boolean sensorDataUpdateOne(SensorData sensrodata);

	/**
	 * センサデータ削除メソッド
	 */
	public boolean sensorDataDeleteOne(String id, String userName);

}