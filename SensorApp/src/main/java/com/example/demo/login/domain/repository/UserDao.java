package com.example.demo.login.domain.repository;


import org.springframework.dao.DataAccessException;

import com.example.demo.login.domain.model.User;


public interface UserDao {

	//Userコレクションからユーザ情報を検索する
	public User userFindOne(String userName) throws DataAccessException;

	//Userコレクションにユーザを追加する
	public void userInsertOne(User user) throws DataAccessException;

	//Userコレクションのユーザを更新する
	public User userUpdateOne(User user) throws DataAccessException;

	//Userコレクションからユーザを削除する
	public int userDeleteOne(User user) throws DataAccessException;

}