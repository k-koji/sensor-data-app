package com.example.demo.login.domain.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.repository.UserDao;

@Service
public class UserService {
	@Autowired
	UserDao dao;

	@Autowired
	PasswordEncoder passwordEncoder;

	/**
	 * ユーザ情報検索メソッド
	 */
	public User userFindOne(String userName) {
		return dao.userFindOne(userName);
	}

	/**
     * ユーザ追加メソッド.
     */
	public boolean userInsertOne(User user) {
		boolean result = false;

		// TBC ユーザ名が既に使われている場合は、画面にその旨を出力させたい。
		if (dao.userFindOne(user.getUserName()) == null) { //ユーザ名が存在しない場合登録処理
			dao.userInsertOne(user);
			if (dao.userFindOne(user.getUserName()) != null) { //登録成功可否
				result = true;
			}
		}

		return result;
	}

	/**
	 * ユーザ更新メソッド
	 */
	public boolean userUpdateOne(User user) {
		boolean result = false;

		if (dao.userUpdateOne(user) != null) { //更新成功
			result = true;
		}

		return result;
	}

	/**
	 * ユーザ削除メソッド
	 */
	public boolean userDeleteOne(User user) {
		boolean result = false;

		//パスワードが正しい場合ユーザを削除する
		String encodedPassword = userFindOne(user.getUserName()).getPassword();
		if(passwordEncoder.matches(user.getPassword(), encodedPassword)) {
			if (dao.userDeleteOne(user) == 1) { //削除成功
				result = true;
			}
		}

		return result;
	}
}