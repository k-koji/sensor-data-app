package com.example.demo.login.domain.repository;


import java.util.List;

import org.springframework.dao.DataAccessException;

import com.example.demo.login.domain.model.SearchSensorDataForm;
import com.example.demo.login.domain.model.SensorData;


public interface SensorDataDao {

	// sensor_dataテーブルからデータを取得
	public SensorData sensorDataFindOne(String id) throws DataAccessException;

	// sensor_dataテーブルからデータリスト取得
	public List<SensorData> sensorDataList(SearchSensorDataForm searchSensorData) throws DataAccessException;

	// sensor_dataテーブルにセンサデータを追加
	public void sensorDataInsertOne(SensorData sensordata) throws DataAccessException;

	// sensor_dataテーブルのセンサデータを更新
	public SensorData sensorDataUpdateOne(SensorData sensordata) throws DataAccessException;

	// sensor_dataテーブルからデータ削除
	public int sensorDataDeleteOne(String id, String userName) throws DataAccessException;
}