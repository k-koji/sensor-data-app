package com.example.demo.login.domain.repository.db;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.login.domain.model.User;
import com.example.demo.login.domain.repository.UserDao;
import com.mongodb.client.result.DeleteResult;

@Repository
public class UserDaoImpl implements UserDao {
	@Autowired
	private MongoTemplate template;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Override
	public User userFindOne(String userName) throws DataAccessException {
		Query query = new Query();
		query.addCriteria(Criteria.where("userName").is(userName));
		User user = template.findOne(query, User.class);

		return user;
	}

	@Override
	@Transactional(readOnly = false)
	public void userInsertOne(User user) throws DataAccessException {
		String password = passwordEncoder.encode(user.getPassword()); //ユーザパスワードは暗号化して保存する
		user.setPassword(password);
		template.save(user);
	}

	@Override
	@Transactional(readOnly = false)
	public User userUpdateOne(User user) throws DataAccessException {

		//更新前のユーザ情報の取得
		Query query = new Query();
		query.addCriteria(Criteria.where("userName").is(user.getUserName()));

		//パスワードを暗号化
		String password = passwordEncoder.encode(user.getPassword());
		user.setPassword(password);

		Update updateUser = new Update();
		updateUser.set("password", password);
		updateUser.set("mailAddress", user.getMailAddress());

		return template.findAndModify(query, updateUser, User.class);
	}

	@Override
	public int userDeleteOne(User user) throws DataAccessException {

		Query query = new Query();
		query.addCriteria(Criteria.where("userName").is(user.getUserName()));
		DeleteResult result = template.remove(query, User.class);

		return (int)result.getDeletedCount();
	}
}