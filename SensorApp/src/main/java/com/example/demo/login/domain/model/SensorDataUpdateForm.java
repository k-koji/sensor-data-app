package com.example.demo.login.domain.model;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "sensor_data")
public class SensorDataUpdateForm implements Serializable{
	@NotBlank
	private String id;
	@NotBlank
	private String sensorType;
	@NotBlank
    private String sensorValue;
	@NotBlank
	private String sensorPos;
}
