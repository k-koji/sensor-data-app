package com.example.demo.login.domain.model;

import java.io.Serializable;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection = "user_data")
public class User implements Serializable{

    private String userName;
    private String password;
    private String mailAddress;
}
