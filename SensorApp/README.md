# センサデータ格納サンプルアプリ

センサデータを格納するためのRESTサービスのサンプルアプリ。  
Spring Boot + MongoDBの勉強用に作成したもの。
  
## 機能
 
- センサデータのアップロード
- センサデータの検索
- センサデータの取得
- センサデータの削除
 
## 必要要件
 
- java 8
- MongoDB v3.6 #動作確認済みバージョン
 
## 事前設定
  
- MongoDBのデータベース・コレクション作成
  - データベース名
    - test
  - コレクション名 
    - sernsor_data
    - user_data
  
  
- application.ymlをMongoDBの設定に合わせて修正
  ```
  spring:
    data:
      mongodb:
        host: 192.168.1.11 
        port: 27017
        database: test
  ```
   
## 使い方
- Webからの操作  
  http://localhost:8080/login

- REST API経由での操作(例)
  - ユーザ管理
    - ユーザ登録 
    ```
    curl http://localhost:8081/rest/user/insert -X POST -H "Content-Type:application/json" -d "{\"userName\":\"test\",\"password\":\"test\",\"mailAddress\":\"test@test.com\"}"
    ```
    - ユーザ情報取得
     ```
     curl http://localhost:8081/rest/user/get -X GET -u test:test
     ```
    - ユーザ情報更新
     ```
     curl http://localhost:8081/rest/user/update -X PUT -H "Content-Type:application/json" -d "{\"userName\":\"test\",\"password\":\"test_modify\",\"mailAddress\":\"test_modify@test.com\"}" -u test:test
     ```
    - ユーザ削除
     ```
     curl http://localhost:8081/rest/user/delete -X DELETE -u test:test
     ```

  - センサデータ管理
    - データ登録
     ```
     curl http://localhost:8081/rest/sensordata/insert -X POST -H "Content-Type:application/json" -d "{\"sensorType\":\"light\",\"sensorValue\":\"100\",\"sensorPos\":\"(132,14)\",\"timestamp\":\"2020-12-28T22:27:00\"}" -u test:test
     ```
    - データ取得
     ```
     curl http://localhost:8081/rest/sensordata/get -X GET -H "Content-Type:application/json" -d "{\"userName\":\"test\",\"sensorType\":\"light\",\"startTimestamp\":\"2020-12-01T00:00:00\",\"endTimestamp\":\"2020-12-31T00:00:00\"}" -u test:test
     ```
    - データ更新
     ```
     curl http://localhost:8081/rest/sensordata/update -X POST -H "Content-Type:application/json" -d "{\"id\":\"5fecce453e5e0483247e96a5\",\"sensorType\":\"light\",\"sensorValue\":\"200\",\"sensorPos\":\"(1321,141)\"}" -u test:test
     ```
    - データ削除
     ```
     curl http://localhost:8081/rest/sensordata/delete/5fc32dd256ab201ea7c5b895 -X DELETE -u test:test
     ```
 

